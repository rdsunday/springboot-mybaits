package demo03.dao;

import demo03.entity.userlogin;

public interface userloginMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(userlogin record);

    int insertSelective(userlogin record);

    userlogin selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(userlogin record);

    int updateByPrimaryKey(userlogin record);
}