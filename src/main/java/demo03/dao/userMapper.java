package demo03.dao;

import demo03.entity.user;
import demo03.entity.userWithBLOBs;

public interface userMapper {
    int deleteByPrimaryKey(String user);

    int insert(userWithBLOBs record);

    int insertSelective(userWithBLOBs record);

    userWithBLOBs selectByPrimaryKey(String user);

    int updateByPrimaryKeySelective(userWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(userWithBLOBs record);

    int updateByPrimaryKey(user record);
}