package demo03;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Demo03Application {

	public void testGenerator() throws IOException, XMLParserException, InvalidConfigurationException, SQLException, InterruptedException {
		List<String> warnings=new ArrayList<String>();
		boolean overWriter=true;
//指向配置文件
		File configFile=new File(Demo03Application.class.getResource("/setting/generatorConfig").getFile());
		ConfigurationParser cp=new ConfigurationParser(warnings);
		Configuration config=cp.parseConfiguration(configFile);
		DefaultShellCallback callback=new DefaultShellCallback(overWriter);
		MyBatisGenerator myBatisGenerator=new MyBatisGenerator(config,callback,warnings);
		myBatisGenerator.generate(null);
	}

	public static void main(String[] args) throws InterruptedException, SQLException, InvalidConfigurationException, XMLParserException, IOException {

		Demo03Application demo03Application =new Demo03Application();
		demo03Application.testGenerator();
		SpringApplication.run(Demo03Application.class, args);
	}

}
